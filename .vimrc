set nocompatible              " be iMproved, required
filetype off                  " required

" ****************** VUNDLE ***********************
"
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo

" Plugin from github
Plugin 'tpope/vim-fugitive'
Plugin 'scrooloose/nerdtree'
Plugin 'ervandew/supertab'
Plugin 'vim-airline/vim-airline' 
Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'
Plugin 'suan/vim-instant-markdown'
Plugin 'easymotion/vim-easymotion'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'sheerun/vim-polyglot'
Plugin 'gregsexton/gitv'
Plugin 'airblade/vim-gitgutter'
Plugin 'szw/vim-dict'
"
" Color
Plugin 'tyrannicaltoucan/vim-quantum'
Plugin 'vim-scripts/desert256.vim'
"
" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
"
"
"
" ****************** INITIALIZE *******************
"
set nocompatible              " be iMproved, required
filetype plugin indent on   " Automatically detect file types.
syntax on                   " syntax highlighting
"
" ****************** VISUAL ***********************
" GVIM- (here instead of .gvimrc)
if has('gui_running')
    color quantum                     " load a colorscheme
    set guifont=Monospace\ 12
endif
set nu
" set cursorline                  " highlight current line
"
" ****************** ENVIRONMENT ******************
set backspace=indent,eol,start                      " backspace for dummies
set showmatch                                       " show matching brackets/parenthesis
set wildmenu                                        " show list instead of just completing
set wildmode=list:longest,full                      " comand <Tab> completion, list matches, then longest common part, then all.
set shortmess+=filmnrxoOtT                          " abbrev. of messages (avoids 'hit enter')
set showmode                                        " display the current mode
set tabpagemax=15                                   " only show 15 tabs
set hlsearch                                        " highlight search terms
set winminheight=0                                  " windows can be 0 line high
set smartcase                                       " case sensitive when uc present
set scrolljump=5                                    " lines to scroll when cursor leaves screen
set scrolloff=3                                     " minimum lines to keep above and below cursor
set mouse=a 										" enable mouse
set foldenable                                      " auto fold code
set foldmethod=marker                               " type of folding
"set foldclose=all
"set writebackup
"set spell                                           " spell checking on
"set backup                                          " backups are nice
"set backupdir=$HOME/.backup                         " but they clog cwd
"set backupdir=~/.backup,~/tmp,.,/tmp"}}}
"set incsearch                                       " find as you type search
"set autowrite
"set ignorecase                                      " case insensitive search
"set whichwrap=b,s,h,l,<,>,[,]                       " backspace and cursor keys wrap to
"
"" ****************** FORMATTING *******************
set nowrap                                          " wrap long lines
set autoindent                                      " indent at the same level of the previous line
set expandtab                                       " tabs are spaces
set shiftwidth=4                                    " use indents of 4 spaces
set tabstop=4                                       " an indentation every four columns
set softtabstop=4
autocmd FileType md,markdown,html,javascript,json setlocal expandtab shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType java,c,cpp,h setlocal shiftwidth=4 tabstop=4 softtabstop=4
autocmd FileType python setlocal expandtab shiftwidth=4 tabstop=8
"autocmd FileType vim setlocal expandtab shiftwidth=2 tabstop=8 softtabstop=2
"autocmd FileType java setlocal shiftwidth=2 tabstop=8 softtabstop=2 expandtab
"set comments=sl:/*,mb:*,elx:*/                      " auto format comment blocks
"set noexpandtab                                     " tabs are tabs, not spaces
"set matchpairs+=<:>                                 " match, to be used with %
"set pastetoggle=<F12>                               " pastetoggle (sane indentation on pastes)
"
"" ****************** GENERAL SHORTCUTS *******************
noremap <S-H> gT
noremap <S-L> gt
nnoremap <Space> <PageDown>
nnoremap <S-Space> <PageUp>
"map <C-N> :cn<CR>
"map <C-P> :cp<CR>
"map <C-L> :cl<CR>
"map <F10> <C-w><S-_>
"
" VCSCommand
"nnoremap <F2> :Search <C-R>=expand("<cword>")<CR><CR>
"nnoremap <F3> :cs f g <C-R>=expand("<cword>")<CR><CR>
"nnoremap <S-F2> :SearchReset<CR>
"nnoremap <S-F3> :cs add .<CR>
"nnoremap <F4> :cs f c <C-R>=expand("<cword>")<CR><CR>
nnoremap <F6> :Git blame<CR>
"nnoremap <C-F6> :Gitv<CR>
nnoremap <F5> :Git log --graph<CR>
"nnoremap <F7> :set nowrap<CR>
"nnoremap <S-F7> :set wrap<CR>
"nnoremap <F8> :tabnew <cfile><CR>
nnoremap <F8> :set syntax=markdown<CR>
nnoremap <F9> :Dict<CR>
nnoremap <C-F11> :InstantMarkdownPreview<CR>
nnoremap <F12> :NERDTreeToggle<CR>
nnoremap <C-F12> :!ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .<CR>
nmap f <Plug>(easymotion-overwin-f2)
" Tabular Usage:
" :Tab /=   " This will align the text with =
"
" ****************** PLUGINS SETTINGS *******************
"let NERDTreeWinPos="right"
let g:vim_markdown_new_list_item_indent = 2
let g:vim_markdown_autowrite = 1
let g:vim_markdown_toc_autofit = 1
let g:vim_markdown_folding_disabled = 1
"let g:vim_markdown_folding_style_pythonic = 2
"let g:vim_markdown_auto_extension_ext = 'txt'
"
let g:instant_markdown_autostart = 0
"let g:instant_markdown_slow = 1
"
"
let g:EasyMotion_smartcase = 1
" 
"let g:netrw_browsex_viewer="setsid xdg-open"
"let g:netrw_http_cmd="elinks"
let g:netrw_http_xcmd= "-dump >"
let g:dict_hosts = [["localhost", ["wn","gcide"]]]
"let g:dict_hosts = [["localhost", ["*"]]]
"Hide the printer option in the ToolBar
:aunmenu ToolBar.Print
