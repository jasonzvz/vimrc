## Table of Contents

* [About](#about)
* [Quick Start](#quick-start)
* [Plugins](#plugins)

## About

This is my personal vimrc configure using [Vundle]

## Quick Start

1. Set up [Vundle]:
    ```sh
    git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
    ```

2. Apply the .vimrc config file:
    - Download the .vimrc file to ~/.vimrc

3. Install Plugins:
    - Launch `vim` and run `:PluginInstall`
    - To install from command line:
      ```sh
      vim +PluginInstall +qall
      ```
    - Other cmds:
      ```vim
      " :PluginList       - lists configured plugins
      " :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
      " :PluginSearch foo - searches for foo; append `!` to refresh local cache
      " :PluginClean
      ```
4. Extra package for vim plugins
    * Plugin [suan/vim-instant-markdown]
      ```sh
      sudo npm -g install instant-markdown-d
      ```
    * Plugin [szw/vim-dict]
      ```sh
      sudo apt install dictd
      sudo apt install dict-wn
      sudo apt install dict-gcide
      ```

## Plugins
* [x] Plugin [tpope/vim-fugitive]
* [x] Plugin [scrooloose/nerdtree]
* [x] Plugin [ervandew/supertab]
* [x] Plugin [vim-airline/vim-airline]
* [x] Plugin [godlygeek/tabular]
* [x] Plugin [plasticboy/vim-markdown]
* [x] Plugin [suan/vim-instant-markdown]
* [x] Plugin [easymotion/vim-easymotion]
* [x] Plugin [ctrlpvim/ctrlp.vim]
* [x] Plugin [sheerun/vim-polyglot]
* [x] Plugin [gregsexton/gitv]
* [x] Plugin [airblade/vim-gitgutter]
* [x] Plugin [szw/vim-dict]
* [x] Plugin [tyrannicaltoucan/vim-quantum]
* [x] Plugin [vim-scripts/desert256.vim]

[Vundle]:http://github.com/VundleVim/Vundle.vim
[tpope/vim-fugitive]:http://github.com/tpope/vim-fugitive
[scrooloose/nerdtree]:http://github.com/scrooloose/nerdtree
[ervandew/supertab]:http://github.com/ervandew/supertab
[vim-airline/vim-airline]:http://github.com/vim-airline/vim-airline
[godlygeek/tabular]:http://github.com/godlygeek/tabular
[plasticboy/vim-markdown]:http://github.com/plasticboy/vim-markdown
[suan/vim-instant-markdown]:http://github.com/suan/vim-instant-markdown
[easymotion/vim-easymotion]:http://github.com/easymotion/vim-easymotion
[ctrlpvim/ctrlp.vim]:http://github.com/ctrlpvim/ctrlp.vim
[sheerun/vim-polyglot]:http://github.com/sheerun/vim-polyglot
[gregsexton/gitv]:http://github.com/gregsexton/gitv
[airblade/vim-gitgutter]:http://github.com/airblade/vim-gitgutter
[szw/vim-dict]:http://github.com/szw/vim-dict
[tyrannicaltoucan/vim-quantum]:http://github.com/tyrannicaltoucan/vim-quantum
[vim-scripts/desert256.vim]:http://github.com/vim-scripts/desert256.vim
